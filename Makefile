CXX=g++ -std=c++11
CXXFLAGS=-g -Wall $(EXTRA)

build: client server

client:main_client.cpp client.cpp file.cpp log.cpp
	 $(CXX) $(CXXFLAGS) -o $@ $^
	
server:server_main.cpp client.cpp file.cpp
	 $(CXX) $(CXXFLAGS) -o $@ $^
	
clean:
	rm -f -R client server *.
