//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "utils.h"

#include "client.h"
#include "file.h"
#include "log.h"

using namespace std;

//numele clientului(folosit in log)
std::string				clientName;

//fisierele partajate si istoricul
std::vector<File>       shared_files;
std::vector<Log>        history;

//vectori auxiliari folositi pentru controlul transferului de fisiere
get_file_transfer       gft[MAXTRANSFER];
share_file_transfer     sft[MAXTRANSFER];
int                     ongoing_input;
int                     ongoing_output;

//multimile de file descriptori pentru citire si scriere, file descriptorul de valoare maxima
int                     fdmax;
fd_set                  read_fds;
fd_set                  write_fds;
//multimile folosite temporar
fd_set                  tmp_read_fds;
fd_set                  tmp_write_fds;

//initializeaza socketul de listen si conexiunea la server
int init(char **argv, int &sockfdc, int &sockfds, std::ofstream &fout)
{
    int                  port_listen;
    int                  port_server;
    int                  response;
    struct sockaddr_in   server_addr;
    struct sockaddr_in   client_addr;
    char                 sendBuf[BUFLEN];


    //creare socketi TCP
    sockfdc = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfdc < 0) {
        std::cerr << "Error opening socket. Exiting.";
        return 1;
    }
    sockfds = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfds < 0) {
        std::cerr << "Error opening socket. Exiting.";
        return 1;
    }

    //portul folosit la listen
    port_listen = atoi(argv[3]);
    //portul serverului
    port_server = atoi(argv[5]);

    std::cout << "Opening listen port on: " << port_listen << std::endl;
    std::cout << "Connecting to the server port: " << port_server << std::endl;

    //pregatire client address
    memset((void *) &client_addr, 0, sizeof(client_addr));
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = INADDR_ANY;	    // foloseste adresa IP a masinii
    client_addr.sin_port = htons(port_listen);

    //pregatire server address
    memset((void *) &server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_server);
    inet_aton(argv[1], &server_addr.sin_addr);

    //asignare socket address la socket
    response = bind(sockfdc, (struct sockaddr *) &client_addr, sizeof(struct sockaddr));
    if (response < 0) {
        std::cerr << "ERROR on binding";
        return 2;
    }

    //conectare la server
    response = connect(sockfds, (struct sockaddr*) &server_addr, sizeof(server_addr));
    if(response < 0) {
        std::cerr << "Error with connection to server." << std::endl;
        fout << clientName << "> ";
        fout << "-1 : Eroare la conectare" << std::endl;
        return 3;
    }

    //trimitere date initiale cu numele serverului si portul pe care asculta
    sprintf(sendBuf, "init %s %d", argv[1], port_listen);
    response = send(sockfds, sendBuf, strlen(sendBuf), 0);
    if(response < 0) {
        std::cerr << "Error in sending initialization data to server.";
        std::cerr << std::endl;
        fout << clientName << "> ";
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return 4;
    }

    if(response == 0) {
        return cmd_quit;
    }

    response = recv(sockfds, sendBuf, sizeof(sendBuf), 0);
    if(response < 0 || !strncmp(sendBuf, "Fail", 4)) {
        std::cerr << "Error in sending initialization data to server.";
        std::cerr << std::endl;
        fout << clientName << "> ";
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return 4;
    }

    //changing working directory
    response = chdir(argv[2]);
    if(response < 0) {
        std::cerr << "Could not open share directory." << std::endl;
        fout << clientName << "> ";
        fout << "Could not open share directory." << std::endl;
        return 5;
    }
    memset(sendBuf, 0, sizeof(sendBuf));
    if(getcwd(sendBuf, sizeof(sendBuf)) == NULL) {
        std::cerr << "Could not get share directory." << std::endl;
        fout << clientName << "> ";
        fout << "Could not get share directory." << std::endl;
        return 6;

    }
    std::cout << "Working directory set to: "
              << sendBuf << std::endl;

    //initializare incheiata cu succes
    return 0;
}

//rezolva cerinta: InfoClients
void handleInfoClients(
        char                    *buffer,
        int                     sockfds,
        std::vector<Client>     &clients,
        std::ofstream           &fout)
{
    char    sendBuf[BUFLEN];
    memset(sendBuf,0, BUFLEN);
    sprintf(sendBuf, "infoclients");
    //trimite cererea la server
    if(send(sockfds, sendBuf, strlen(sendBuf), 0) < 0) {
        std::cerr << "Fail to send data to server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        fout.flush();
        return;
    }
    //primeste raspunsul si il afiseaza
    memset(sendBuf, 0, BUFLEN);
    if(recv(sockfds, sendBuf, sizeof(sendBuf), 0) < 0) {
        std::cerr << "Failed to receive data from server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        fout.flush();
        return;
    }
    std::cout << "Server responded with: " << sendBuf << std::endl;
    std::istringstream  in(sendBuf);
    char name[MAXLEN], ip[IPV4SIZE], port[MAXLEN];
    clients.clear();
    //parseaza raspunsul si il pune in log
    fout << "infoclients" << std::endl;
    while((in >> name>> ip >> port) != NULL) {
        Client c;
        c.getName().assign(name);
        c.getIP().assign(ip);
        c.getPort().assign(port);
        clients.push_back(c);
        std::cout << "Added data for client: " << c.log();
        fout << c.log();
    }
    fout.flush();
}

//rezolva cerinta: share
void handleShare(
    char            *buffer,
    int             sockfds,
    std::ofstream   &fout)
{
    char    sendBuf[BUFLEN];
    char    cmd[MAXLEN], fname[MAXLEN];
    std::istringstream  in(buffer);
    in >> cmd >> fname;
    std::cout << "Command: " << cmd << " " << fname << std::endl;
    fout << clientName << "> ";
    fout << cmd << " " << fname << std::endl;
    struct stat fileStat;
    //verifica existenta fisierului
    if(stat(fname, &fileStat) < 0) {
        std::cerr << "Fisier inexistent" << std::endl;
        fout << "-5 : Fisier inexistent" << std::endl;
        return;
    }
    File f(fname, fileStat.st_size);
    bool found = false;
    //cauta daca deja a fost partajat fisierul respectiv
    for(auto &file: shared_files)
        if( !strcmp(file.getName().c_str(), fname) ) {found = true; break;}
    if(found) {
        std::cerr << "Fisier partajat cu acelasi nume" << std::endl;
        fout << "-6 : Fisier partajat cu acelasi nume" << std::endl;
        return;
    }
    memset(sendBuf, 0, BUFLEN);
    //trimite informatia la server
    sprintf(sendBuf, "share %s %lu", fname, fileStat.st_size);
    std::cout << "Sending request  to server: "
              << "share " << f.getName().c_str();
    std::cout << " " << f.getSize() << std::endl;
    if(send(sockfds, sendBuf, strlen(sendBuf), 0) < 0) {
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        std::cerr << "Error sending data to server." << std::endl;
        return;
    }
  	//primeste raspunsul verifica daca a primit eroare
    memset(sendBuf, 0, BUFLEN);
    if(recv(sockfds, sendBuf, sizeof(sendBuf), 0) < 0) {
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        std::cerr << "Error receiving data to server." << std::endl;
        return;
    }
    std::cout << "Got from response from server: " << sendBuf << std::endl;
    if(!strncmp(sendBuf, "Success", strlen("Success"))) {
        std::cout << "Succes" << std::endl;
        fout << "Succes" << std::endl;	
        shared_files.push_back(f);
        return;
    }
}

//rezolva cerinta getShare
void handleGetShare(
    char                *buffer,
    int                 sockfds,
    std::ofstream       &fout,
    std::vector<Client> &clients)
{
    char        sendBuf[BUFLEN];
    std::istringstream in(buffer);
    char        dmp[MAXLEN], clientName[MAXLEN];
    in >> dmp >> clientName;
    std::cout << "Command is: " << dmp << " " << clientName << std::endl;
    sprintf(sendBuf, "getshare %s", clientName);
    fout << clientName << "> ";
    fout << sendBuf << std::endl;
    //trimite cererea la server
    if(send(sockfds, sendBuf, strlen(sendBuf),0) < 0 ){
        std::cerr << "Error sending data to server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return;
    }
    memset(sendBuf, 0, sizeof(sendBuf));
    //porimeste raspunsul
    if(recv(sockfds, sendBuf, sizeof(sendBuf), 0) < 0) {
        std::cerr << "Error receiving data from server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return;
    }
    //verifica daca a primit eroare
    std::cout << "Got server response: " << sendBuf << std::endl;
    if(!strncmp(sendBuf, "-3", strlen("-3"))) {
        std::cerr << "Client inexistent." << std::endl;
        fout << "-3 : Client inexistent" << std::endl;
        return;
    }
    int n;
    std::string name, fsize, unit_fsize;
    in.clear();
    in.str(sendBuf);
    in >> n;
    fout << n << std::endl;
    //pregateste vectorii pentru reactualizare
    for(auto &client: clients)
     	if(!strcmp(client.getName().c_str(), clientName)) {
      	  client.getShared_files().clear();break;
      }
    //parseaza informatiile de la server, le afiseaza
    for(int i = 0; i < n; ++i) {
        in >> name >> fsize >> unit_fsize;
        File f(name.c_str(), atol(fsize.c_str()), unit_fsize.c_str());
        for(auto& client: clients) {
            if(!strcmp(client.getName().c_str(), clientName)) {
                client.getShared_files().push_back(f);
                File aux = client.getShared_files().back();
                std::cout << "Added: " << aux.getName() << " "
                          << aux.translateSize() <<std::endl;
                break;
            }
        }
        fout << name << " " << fsize << unit_fsize << std::endl;
    }
    fout.flush();
}

//rezolva cerinta unshare
void handleUnshare(
    char            *buffer,
    int             sockfds,
    std::ofstream   &fout)
{
    char        sendBuf[BUFLEN];
    std::istringstream in(buffer);
    char        dmp[MAXLEN], fileName[MAXLEN];
    in >> dmp >> fileName;
    std::cout << "Command is: " << dmp << " " << fileName << std::endl;
    sprintf(sendBuf, "unshare %s", fileName);
    fout << clientName << "> ";
    fout << sendBuf << std::endl;
    //trimite cererea la server
    if(send(sockfds, sendBuf, strlen(sendBuf),0) < 0 ){
        std::cerr << "Error sending data to server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return;
    }
    //primeste raspunsul
    memset(sendBuf, 0, sizeof(sendBuf));
    if(recv(sockfds, sendBuf, sizeof(sendBuf), 0) < 0) {
        std::cerr << "Error receiving data from server." << std::endl;
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return;
    }
    //verifica daca a primit eroare
    std::cout << "Got server response: " << sendBuf << std::endl;
    if(!strncmp(sendBuf, "-5", strlen("-5"))) {
        std::cerr << "Fisier inexistent." << std::endl;
        fout << "-5 : Fisier inexistent" << std::endl;
        return;
    }
    fout << "Succes" << std::endl;
    std::vector<File>::iterator it = shared_files.begin();
    std::vector<std::vector<File>::iterator> positions;
    //scoate din vectorul de fisiere partajate
    for(; it != shared_files.end(); ++it)
        if(!strcmp((*it).getName().c_str(), fileName)){
            positions.push_back(it);
            std::cout << "removed" << (*it).getName() << " " << (*it).translateSize() << std::endl;
        }
    for(auto &itt: positions) shared_files.erase(itt);
    std::cout << "Shared files left are:" << std::endl;
    for(auto &file: shared_files)
        std::cout << file.getName() << " " << file.translateSize() << std::endl;
}

//rezolva cerinta getfile
void handleGetFile(
        char                *buffer,
        int                 sockfds,
        std::ofstream       &fout,
        std::vector<Client> &clients)
{
    std::istringstream      in(buffer);
    std::string             cmd;
    std::string             fileName, clientName;
    char                    buf[BUFLEN];
    memset(buf, 0, BUFLEN);
    in >> cmd >> clientName >> fileName;
    bool found = false;
    Client ctarget;
    //cauta clientul
    for(auto &c: clients) {
        if(!c.getName().compare(clientName)) {ctarget = c; found=true; break;}
    }
    //verifica daca l-a gasit
    if(!found) {
        fout << clientName << "> ";
        fout << cmd << " " << clientName << " " << fileName << std::endl;
        std::cerr << "Client not found." << std::endl;
        fout << "-4 : Client necunoscut" << std::endl;
        return;
    } else std::cout << "Found client: " << ctarget.log();
    found = false;
    File ftarget;
    for(auto &f: ctarget.getShared_files()) {
        if(!f.getName().compare(fileName)) {ftarget = f; found = true; break;}
    }

    found = false;
    for(auto &f: shared_files) {
        if(!f.getName().compare(fileName))  {found = true; break;}
    }
    //verifica daca a partajat un fisier cu acelasi nume
    if(found) {
        fout << clientName << "> ";
        fout << cmd << " " << clientName << " " << fileName << std::endl;
        fout << "-6 : Fisier partajat cu acelasi nume" << std::endl;
        std::cerr << "File with same name already shared." << std::endl;
        return;
    }
    //cauta un slot de transfer liber
    for(int i = 0; i < MAXTRANSFER; ++i) {
        if(gft[i].active) continue;
        //deschid conexiune catre clientul de la care doresc sa iau fisierul
        gft[i].socket = socket(AF_INET, SOCK_STREAM, 0);
        if(gft[i].socket < 0) {
            fout << clientName << "> ";
            fout << cmd << " " << clientName << " " << fileName << std::endl;
            fout << "-2 : Eroare la citire/scriere socket" << std::endl;
            std::cout << "Socket error." << std::endl;
            return;
        }
        struct sockaddr_in peer_addr;
        memset((char *) &peer_addr, 0, sizeof(peer_addr));
        peer_addr.sin_family = AF_INET;
        peer_addr.sin_port = htons(atoi(ctarget.getPort().c_str()));
        inet_aton(ctarget.getIP().c_str(), &peer_addr.sin_addr);

        if (connect(gft[i].socket,(struct sockaddr*) &peer_addr,sizeof(peer_addr)) < 0) {
            std::cout << "Cannot connect to peer." << std::endl;
            fout << clientName << "> ";
            fout << cmd << " " << clientName << " " << fileName << std::endl;
            fout << "-1 : Eroare la conectare\n" << std::endl;
            return;
        }
        //conexiune realizata cu succes
        //se trimite mesajul de initializare transfer
        sprintf(buf, "getfile %s", fileName.c_str());

        if((send(gft[i].socket, buffer, strlen(buffer),0)) < 0) {
            std::cout << "Error sending message to peer." << std::endl;
            fout << clientName << "> ";
            fout << cmd << " " << clientName << " " << fileName << std::endl;
            fout << "-2 : Eroare la citire/scriere socket" << std::endl;
            return;
        }

        memset(buf ,0, sizeof(buffer));
        //se primeste raspunsul de la celalalt client
        if((recv(gft[i].socket, buf, sizeof(buffer),0)) < 0) {
            std::cout << "Error sending message to peer." << std::endl;
            fout << clientName << "> "; 
            fout << cmd << " " << clientName << " " << fileName << std::endl;
            fout << "-2 : Eroare la citire/scriere socket" << std::endl;
            return;
        }
        //verifica daca a primit cod de eroare
        if(!strncmp(buf, "-5", 2)) {
            std::cout << "Fisier inexistent." << std::endl;
            fout << clientName << "> ";
            fout << cmd << " " << clientName << " " << fileName << std::endl;
            fout << "-5 : Fisier inexistent" << std::endl;
            return;
        } else if(!strncmp(buf, "+5", 2)) {
        	//clientul celalalt confirma cererea. se umple slotul de tranfser
            gft[i].active = true;
            gft[i].client = clientName;
            gft[i].file = fileName;
            FD_SET(gft[i].socket, &read_fds);
            fdmax = std::max(fdmax, gft[i].socket);
            gft[i].f.open(gft[i].file, std::ofstream::binary);
            ++ongoing_input;
            break;
        }
    }
}

//rezolva cerinta history
void handleHistory(std::ofstream    &fout) {
	//parcurge logurile si le afiseaza
    fout << clientName << "> ";
    fout << "history" << std::endl;
    std::cout << "history" << std::endl;
    fout << history.size() << std::endl;
    std::cout << history.size() << std::endl;
    for(auto &log: history) {
        std::cout << log << std::endl;
        fout << log << std::endl;
    }
    fout.flush();
}

//primeste input de la user
int handleUserInput(
        int                     sockfds,
        std::vector<Client>     &clients,
        std::ofstream           &fout,
        char                    *myname)
{
    char                        buffer[BUFLEN];
    fgets(buffer, BUFLEN - 1, stdin);
    //verifica ce comanda a primit
    if(!strncmp(buffer, "quit", 4)) {
        memset(buffer ,0, sizeof(buffer));
        sprintf(buffer, "quit");
        //notifica serverul ca va renunta
        std::cout << "Client will terminate. Notifying server." << std::endl;
        if(send(sockfds, buffer, strlen(buffer), 0)<0) {
            fout << "-2 : Eroare la citire/scriere pe socket" << endl;
            std::cout << "Failed to send data to server" << std::endl;
            return -2;
        }
        fout << clientName << "> ";
        fout << "quit" << std::endl;
        fout.flush();
        std::cout << std::endl;
        return cmd_quit;
    }
    //infoclients
    if(!strncmp(buffer, "infoclients", strlen("infoclients"))) {
        std::cout << "Command is: infoclients" << std::endl;
        fout << clientName << "> ";
        handleInfoClients(buffer, sockfds, clients, fout);
        fout.flush();
        return 0;
    }
    //share
    if(!strncmp(buffer, "share", strlen("share"))) {
        std::cout << "Command is: share" << std::endl;
        handleShare(buffer, sockfds, fout);
        fout.flush();
        return 0;
    }
    //getshare
    if(!strncmp(buffer, "getshare", strlen("getshare"))) {
        std::cout << "Command is: getshare" << std::endl;
        handleGetShare(buffer, sockfds, fout, clients);
        fout.flush();
        return 0;
    }
    //unshare
    if(!strncmp(buffer, "unshare", strlen("unshare"))) {
        std::cout << "Command is: unshare" << std::endl;
        handleUnshare(buffer, sockfds, fout);
        fout.flush();
        return 0;
    }
    //getfile
    if(!strncmp(buffer, "getfile", strlen("getfile"))) {
        std::cout << "Command is: getfile" << std::endl;
        handleGetFile(buffer, sockfds, fout, clients);
        fout.flush();
        return 0;
    }
    //history
    if(!strncmp(buffer, "history", strlen("history"))) {
        std::cout << "Command is: history" << std::endl;
        handleHistory(fout);
        fout.flush();
        return 0;
    }
    //nu a primit o comanda valida
    std::cout << "Invalid command." << std::endl;

    return 1;
}

//primeste un mesaj de la server. singurul mesaj valid este quit
int handleServerResponse(
        int                        socket,
        std::vector<Client>        clients,
        std::vector<Log>           logs,
        std::ofstream              &fout)
{
    int                                   response;
    char                                  buffer[BUFLEN];
    char                                  bufferCopy[BUFLEN];
    char*                                 cmd;
    std::string                           dump;

    response =  recv(socket, buffer, sizeof(buffer), 0);
    if (response < 0) {
        std::cerr << "Error on reading from socket";
        fout << clientName << "> ";
        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
        return -2;
    }

    memcpy(bufferCopy, buffer, sizeof(buffer));
    cmd = strtok(bufferCopy, " \n\r\t");
    //daca a primit quit, inchide conexiunile si se pregateste sa iasa
    if(!strncmp(cmd, "quit", strlen("quit"))) {
        fout << clientName << "> ";
        fout << "quit" << std::endl;
    	std::cout << "Server has quit" << std::endl;
        fout.flush();
        return cmd_quit;
    }

    return 0;
}

//primeste input de la unul din clienti
void handleClientInput(
            int             socket,
            std::ofstream   &fout)
{
    char                   buffer[BUFLEN];
    int                    response;
    std::string            dmp;
    std::string            fileName;
    std::string            myName;
    memset(buffer, 0, BUFLEN);
    if ((response = recv(socket, buffer, sizeof(buffer), 0)) <= 0) {
        if (response == 0) {
            //conexiunea s-a inchis, s-a terminat de trimis un fisier
            std::cerr << "Socket hung up." << std::endl;
            FD_CLR(socket, &read_fds);
            FD_CLR(socket, &write_fds);
            for(int i = 0; i < MAXTRANSFER; ++i) {
                if(gft[i].socket == socket) {
                	//se elibereaza slot-ul si se scrie in log
                    gft[i].f.close();
                    gft[i].active = false;
                    fout << clientName << "> ";
                    fout << "getfile " << gft[i].client;
                    fout << " " << gft[i].file << std::endl;
                    fout << "Succes " << gft[i].file << std::endl;
                    std::cout << "File transfer finished: ";
                    std::cout << gft[i].client << " " << gft[i].file << std::endl;
                    history.push_back(Log(gft[i].client.c_str(), gft[i].file.c_str()));
                    --ongoing_input;
                    fout.flush();
                    return;
                }
            }
        } else {
        	fout << clientName << "> ";
            fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
            return;
        }
    }

    // a primit o cerere de getfile de la alt client
    if(!strncmp(buffer, "getfile", strlen("getfile"))) {
        //initializez conexiune
        std::istringstream     in(buffer);
        in >> dmp >> myName >> fileName;
        std::cout << "Am primit: " << buffer << std::endl;
        for(int i = 0; i < MAXTRANSFER; ++i) {
            if(!sft[i].active) {
            	//verifica daca fisierul cerut este partajat
                char sbuf[BUFLEN];
                memset(sbuf, 0, BUFLEN);   
                int found = 0;
                std::cout << "Looking for file: " << fileName << std::endl;
                for(auto &f: shared_files) {
                    if(!f.getName().compare(fileName)) {found = 1; break;}
                }
                if(!found){
                    //fisierul nu s-a gasit
                    std::cout << "Trimit -5 la client. Nu exista fisierul." << std::endl;
                    sprintf(sbuf, "-5");
                    if(send(socket, sbuf, strlen(sbuf), 0) <0){
                        fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                    }
                    FD_CLR(socket, &read_fds);
                    FD_CLR(socket, &write_fds);
                    close(socket);
                    return;
                }
                //fisierul s-a gasit
                //se trimite confirmare la celalat client
                sft[i].active = true;
                sft[i].f.open(fileName, std::ifstream::binary);
                sft[i].file = fileName;
                sft[i].socket = socket;
                FD_SET(socket, &write_fds);
                ++ongoing_output;
                std::cout << "Fisierul exista. trimit confirmare." << endl;
                memset(sbuf, 0, BUFLEN);
                sprintf(sbuf, "+5");
                if(send(socket, sbuf, strlen(sbuf), 0) <0){
                    fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                    return;
                }
                return;
            }
        }
    } else {
        //am primit date, caut transferul curent si scriu in fisier
        for(int i = 0; i < MAXTRANSFER; ++i) {
            if(gft[i].socket == socket) {
                gft[i].f.write(buffer, response);
            }
        }
    }
}

//trimite un payload la un alt client
void sendToClient(
    int             socket,
    std::ofstream   &fout)
{
    char                buffer[BUFLEN];
    int                 response;
    memset(buffer, 0, BUFLEN);
    for(int i = 0; i < MAXTRANSFER; ++i) {
        if(sft[i].active && sft[i].socket == socket) {
        	//cauta slot-ul curent de transfer
            sft[i].f.read(buffer, BUFLEN);
            if(sft[i].f) {
                //bloc complet de 1024
                response = send(socket, buffer, BUFLEN, 0);
                if(response < 0) {
                    fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                    std::cout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                    sft[i].active = false;
                    sft[i].f.close();
                    close(socket);
                    return;
                }
            } else {
                //ultimul bloc de date
                response = send(socket, buffer, sft[i].f.gcount(), 0);
                if(response < 0) {
                    fout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                    std::cout << "-2 : Eroare la citire/scriere pe socket" << std::endl;
                }
                sft[i].f.close();
                sft[i].active = false;
                --ongoing_output;
                std::cout << "Finished sending file." << std::endl;
                close(socket);
                FD_CLR(socket, &read_fds);
                FD_CLR(socket, &write_fds);
            }
            break;
        }
    }
}

//bucla principala
int loop(int sockfdc, int sockfds, ofstream &fout, char *myname)
{
    //vectorul de clienti folosit pentru toate operatiile cerute
    std::vector<Client>    clients;
    //vectorul de log folosit pentru istoric
    std::vector<Log>       logs;
    int                    newsockfd;
    int                    response;
    unsigned int           clilen;

    struct sockaddr_in     cli_addr;

    //false daca serverul a primit comanda de quit de la stdin, true altfel
    bool                   terminate = false;


    //se incepe ascultarea pe port
    listen(sockfdc, MAX_CLIENTS);

    //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
    FD_SET(sockfdc, &read_fds);
    //file descriptor-ul 0 reprezinta stdin, folosit pentru comanda quit
    FD_SET(0, &read_fds);
    //adaugam file descriptorul pentru server in multimea read_fds
    FD_SET(sockfds, &read_fds);

    fdmax = std::max(sockfds, sockfdc);

    // main loop
    while (!terminate || ongoing_input || ongoing_output) {
        tmp_read_fds = read_fds;
        tmp_write_fds = write_fds;
        //selecteaza fd pentru read/write
        response = select(fdmax + 1, &tmp_read_fds, &tmp_write_fds, NULL, NULL);
		if (response == -1) {
			std::cerr << "ERROR in select";
			fout << clientName << "> ";
			fout << "-1 : Eroare la conectare" << std::endl;
			close(sockfdc);
			return 3;
        }

        if(FD_ISSET(0, &tmp_read_fds)) {
            //input de la utilizator
            response = handleUserInput(sockfds, clients, fout, myname);
            if(response == cmd_quit) terminate = true;
        }

    	for(int i = 1; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_read_fds)) {
				if (i == sockfdc) {
                    //s-a conectat un client nou
                    clilen = sizeof(cli_addr);
                    if ((newsockfd = accept(sockfdc,(struct sockaddr *)&cli_addr,
						 &clilen)) == -1) {
                        std::cout << "Error accepting connection." << std::endl;
        	            fout << clientName << "> ";
						fout << "-1 : Eroare la conectare" << std::endl;
					}
					else {
						//adaug noul socket intors de accept()
						// la multimea descriptorilor de citire
						FD_SET(newsockfd, &read_fds);
						FD_SET(newsockfd, &write_fds);
						fdmax = std::max(fdmax, newsockfd);
					}
					printf("Noua conexiune de la%s,port %d, socket_client %d\n",
						 inet_ntoa(cli_addr.sin_addr),ntohs(cli_addr.sin_port),
						 newsockfd);
				} else if (i == sockfds) {
                    response = handleServerResponse(i, clients, logs, fout);
                    if(response == cmd_quit) terminate = true;
                    //am primit informatii de la server
				} else {
                    handleClientInput(i, fout);
				}
			} 
            if(FD_ISSET(i, &tmp_write_fds)) {
                //trimit un nou payload la peer
                sendToClient(i, fout);
			}
		}
    }
    //incheierea programului
    fout.close();
    close(sockfdc);
    return 0;
}

int main(int argc, char *argv[])
{
    int                 response;
    int                 sockfdc;
    int                 sockfds;

    //usage
    if(argc != 6) {
        std::cerr << "Wrong parameters. Usage: ./" << argv[0];
        std::cerr<< " <nume_client> "
                 << "<nume_director> "
                 << "<port_client> "
                 << "<ip_server> "
                 << "<port_server>" <<  std::endl;
        return 1;
    }

    //se deschide fisierul de log
    std::ofstream fout(std::string(argv[1]) + ".log");
    std::cout << "Opened log file: " << (std::string(argv[1]) + ".log")
              << std::endl;

    clientName = argv[1];

    //se initializeaza sockets si conexiuni
    response = init(argv, sockfdc, sockfds, fout);
    if(response != 0) {
        fout.close();
        return response;
    }
    //incepe bucla principala
    response = loop(sockfdc, sockfds, fout, argv[1]);
    if(response != 0) {
        fout.close();
        return response;
    }

    fout.close();
    return 0;
}

