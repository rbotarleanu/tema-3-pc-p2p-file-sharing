//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#pragma once
#include "utils.h"

class Log {
private:
	//nume fisier si nume client
    std::string file;
    std::string client;
public:
    Log();
    Log(const char* client, const char* file);
    Log(const Log &source);
    std::string & getFile() { return file; }
    std::string & getClient() { return client; }
    ~Log();
    Log& operator=(const Log& source);
//pentru afisarea istoricului
friend std::ostream& operator<<(std::ostream& out, const Log& log) {
    out << log.client << " " << log.file;
    return out;
}
};
