//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "utils.h"

#include "client.h"

Client::Client() {
    this->socket = -1;
}

//clone constructor
Client::Client(const Client &source) {
    this->name = source.name;
    this->ip = source.ip;
    this->socket = source.socket;
    this->port = source.port;
    this->shared_files = source.shared_files;
}

//copy assignment operator
Client&
Client::operator =(const Client& source){
    //verifica pentru asignare eronata
    if(this != &source) {
        this->name = source.name;
        this->ip = source.ip;
        this->socket = source.socket;
        this->port = source.port;
        this->shared_files = source.shared_files;
	}
	//intoarce noua identitate
	return *this;
}

//afisare informatii pentru infoclients
std::string
Client::log() const {
    std::ostringstream out;
    if(name.length() == 0) out << "UNKNOWN" << " ";
    else out << name << " ";
    out << ip << " ";
    out << port << " " << std::endl;
    return out.str();
}

int
Client::removeFile(std::string fname)
{
    std::vector<File>::iterator     it;
    std::vector<std::vector<File>::iterator> positions;
    for(it = shared_files.begin(); it != shared_files.end(); ++it) {
        if(strcmp((*it).getName().c_str(), fname.c_str())) continue;
        positions.push_back(it);
    }
    if(positions.size() == 0) return 1;
    for(auto &it: positions) shared_files.erase(it);
    return 0;
}
