//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "file.h"

File::File() {
    this->fileSize = 0;
}

File::File(const char *name, long fileSize) {
    this->name = name;
    this->fileSize = fileSize;
}

File::File(const File &source) {
    this->name = source.name;
    this->fileSize = source.fileSize;
}

//translateaza dimensiunea dupa format "uman" in bytes
File::File(const char *name, long fileSize, const char* size_unit) {
    this->name = name;
    if(!strncmp(size_unit, "B", 1)) {
        this->fileSize = fileSize;
    } else if(!strncmp(size_unit, "KiB", 3)) {
        this->fileSize = fileSize * KiB;
    } else if(!strncmp(size_unit, "MiB", 3)) {
        this->fileSize = fileSize * MiB;
    } else {
        this->fileSize = fileSize * GiB;
    }
}

//parseaza dimensiunea intr-un format human readable
const char*
File::translateSize() const {
    std::ostringstream sOut;
    if(fileSize < KiB) sOut << fileSize << " B";
    if(fileSize >= KiB && fileSize < MiB) sOut << fileSize/KiB << " KiB";
    if(fileSize >= MiB && fileSize < GiB) sOut << fileSize/MiB << " MiB";
    if(fileSize >= GiB) sOut << fileSize/GiB << " GiB";
    return sOut.str().c_str();
}

//copy assignment operator
File&
File::operator =(const File &source) {
    //verifica pentru asignare eronata
    if(this != &source) {
        this->name = source.name;
        this->fileSize = source.fileSize;
    }
    //intoarce noua identitate
    return *this;
}

//egalitate intre fisiere
bool
File::operator ==(const File &source) {
    return name.compare(source.name) && this->fileSize == source.fileSize;
}
