//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#pragma once
#include "utils.h"
#include "file.h"

class File;

class Client {
private:
	//numele clientului
    std::string name;
    //ip
    std::string ip;
    //socket folosit de executabilul curent
    int socket;
    //portul pe care asculta
    std::string port;
    //fisierele partajate
    std::vector<File> shared_files;
public:
    //constructor default
    Client();
    //constructorul de clonare
    Client(const Client &source);

    //getteri si setteri pentru atribute
    std::string &getName() {return name;}
    std::string &getIP() {return ip;}
    int getSocket() const {return socket;}
    void setSocket(int socket) {this->socket = socket;}
    std::string &getPort() {return port;}
    std::vector<File> &getShared_files() {return shared_files;}

    //copy assignment operator
    Client& operator =(const Client& source);
    //destructor
    ~Client() {}

    //pentru logging, va intoarce un string cu datele clientului
    std::string log() const;

    //va sterge fisierul din lista de fisiere partajate
    int removeFile(const std::string fname);
};
