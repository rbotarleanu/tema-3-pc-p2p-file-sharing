//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "utils.h"

#include "client.h"
#include "file.h"

//elimina un client din vectorul de clienti(dupa socket-ul pe care este activ)
int removeClientBySocket(
        std::vector<Client>     &clients,
        int                     socket);

//initializeaza hashmap-ul folosit pentru parsarea comenzii
void initMap(std::map<std::string,int> &switchMap);
//initializeaza socket-ul de listen
int handleInit(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);
//rezolva cerinta infoclients
int handleInfoClients(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);
//rezolva cerinta share
int handleShare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);
//rezolva cerinta unshare
int handleUnshare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);
//rezolva cerinta getshare
int handleGetShare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);
//rezolva cerinta quit
int handleQuit(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket);


int init(char *port, int &sockfd)
{
    int                  port_listen;
    int                  response;
    struct sockaddr_in   server_addr;

    //creare socket TCP
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) {
        std::cerr << "Error opening socket. Exiting.";
        return 1;
    }

    //portul folosit la listen
    port_listen = atoi(port);

    //pregatire server address
    memset((void *) &server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;	    // foloseste adresa IP a masinii
    server_addr.sin_port = htons(port_listen);

    //asignare socket address la socket
    response = bind(sockfd, (struct sockaddr *) &server_addr, sizeof(struct sockaddr));
    if (response < 0) {
        std::cerr << "ERROR on binding";
        return 2;
    }

    //initializare incheiata cu succes
    return 0;
}

int newClientHandle(
        unsigned int        &clilen,
        int                 &newsockfd,
        int                 sockfd,
        struct sockaddr_in  &cli_addr,
        int                 &fdmax,
        fd_set              &read_fds,
        std::vector<Client> &clients)
{
    // s-a conectat un nou client
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
	if (newsockfd == -1) {
		std::cerr << "ERROR in accept";
		return 4;
	}
	//adaug noul socket intors de accept() la multimea descriptorilor de citire
	FD_SET(newsockfd, &read_fds);
	fdmax = std::max(fdmax, newsockfd);

    //adaug noul client
    Client *c = new Client();
    c->getIP().assign(inet_ntoa(cli_addr.sin_addr));
    c->setSocket(newsockfd);
    clients.push_back(*c);
    return 0;
}

//input de la utilizator, singura optiune este "quit"
int handleUserInput(
        fd_set                  read_fds,
        std::vector<Client>     &clients)
{
    char                        buffer[BUFLEN];
    fgets(buffer, BUFLEN - 1, stdin);
    //a primit quit
    if(!strncmp(buffer, "quit", 4)) {
        memset(buffer ,0, sizeof(buffer));
        sprintf(buffer, "quit");
        std::cout << "Server will terminate. Notifyng clients." << std::endl;
        //notifica toti clientii
        for(auto &c: clients) {
            send(c.getSocket(), buffer, strlen(buffer), 0);
            std::cout << ".";
        }
        std::cout << std::endl << "All clients have been notified. Exiting.";
        std::cout << std::endl;
        return 0;
    } else {
    	//nu exista alta comanda valida
        std::cout << "Invalid command." << std::endl;
    }
    return 1;
}

//determina evenimentul ce a avut loc(input de la un client)
void handleEvents(
    const char*               buffer,
    const int                 i,
    std::vector<Client>       &clients,
    std::map<std::string,int> switchmap)
{
    std::istringstream      in(buffer);
    std::string             cmd;
    in >> cmd;
    std::cerr << "Command is: " << cmd << std::endl;
    if(switchmap.find(cmd) == switchmap.end()) {
    	//comanda nu se afla in hashmap
        std::cerr << "Invalid command" << std::endl;
        return;
    }
    switch(switchmap.find(cmd)->second) {
        case cmd_initialize: {
        	//un client a trimis datele de initializare
            handleInit(buffer, clients, i);
            break;
        }
        case cmd_infoclients: {
        	//un client a trimis infoclients
            handleInfoClients(buffer, clients,i);
            break;
        }
        case cmd_getshare:{
        	//un client a trimis getshare
            handleGetShare(buffer, clients, i);
            break;
        }
        case cmd_share:{
        	//un cilent a trimis share
            handleShare(buffer, clients, i);
            break;
        }
        case cmd_unshare:{
        	//un client a trimis unshare
            handleUnshare(buffer, clients, i);
            break;
        }
        case cmd_quit:{
        	//un client a trimis quit
            handleQuit(buffer, clients, i);
            break;
        }
    }

}

//un client nou s-a conectat
int newMessageHandle(
        int                 i,
        fd_set              &read_fds,
        std::vector<Client> &clients)
{
    //folosit pentru determinarea comenzii
    std::map<std::string, int> switchmap;
    initMap(switchmap);
    char                   buffer[BUFLEN];
    int                    response;
    memset(buffer, 0, BUFLEN);
    response = recv(i, buffer, sizeof(buffer), 0);
    if (response <= 0) {
        if (response == 0) {
            //conexiunea s-a inchis
            std::cerr << "Socket hung up: " << i << std::endl;
            //scoatem clientul cu socketul dat din vectorul de clienti
            response = removeClientBySocket(clients, i);
            if(response) std::cerr << "Client not found in vector." << std::endl;
                else {
                    std::cerr << "Client removed." << std::endl;
                    std::cerr << "Remaining: " << clients.size() << std::endl;
            }
        } else {
            std::cerr << "ERROR in recv on socket: " << i << std::endl;
            return 5;
        }
        close(i);

        //scoatem din multimea de citire socketul care s-a inchis
        FD_CLR(i, &read_fds);
        return 0;
    } else { //recv intoarce >0
        //in functie de mesajul primit hotaram actiunea pe care o va face serverul
        std::cout << "Am primit de la clientul de pe socketul " << i
                    << " cererea: " << buffer << std::endl;
        handleEvents(buffer, i, clients, switchmap);
    }
    return 0;
}

//main loop
int loop(int sockfd)
{
    //vectorul de clienti folosit pentru toate operatiile cerute
    std::vector<Client>    clients;
    int                    newsockfd;
    int                    response;
    unsigned int           clilen;

    struct sockaddr_in     cli_addr;

    //multimea de citire folosita in select()
    fd_set                 read_fds;
    //multime folosita temporar
    fd_set                 tmp_fds;
    //valoare maxima file descriptor din multimea read_fds
    int                    fdmax;
    //false daca serverul a primit comanda de quit de la stdin, true altfel
    bool                   terminate = false;


    //se incepe ascultarea pe port
    listen(sockfd, MAX_CLIENTS);

    //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
    FD_SET(sockfd, &read_fds);
    //file descriptor-ul 0 reprezinta stdin, folosit pentru comanda quit
    FD_SET(0, &read_fds);

    fdmax = sockfd;

    // main loop
    while (!terminate) {
        tmp_fds = read_fds;
        response = select(fdmax + 1, &tmp_fds, NULL, NULL, NULL);
		if (response == -1) {
			std::cerr << "ERROR in select";
			close(sockfd);
			return 3;
        }

        if(FD_ISSET(0, &tmp_fds)) {
            //input de la utilizator
            response = handleUserInput(read_fds, clients);
            if(!response) terminate = true;
        }

    	for(int i = 1; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				if (i == sockfd) {
                    //s-a conectat un client nou
                    response =newClientHandle(
                                    clilen,
                                    newsockfd,
                                    sockfd,
                                    cli_addr,
                                    fdmax,
                                    read_fds,
                                    clients);
                    if(response) {close(sockfd); return response;}
                    std::cout << clients.back().log();
                    continue;
				} else {
                    //am primit informatii de la unul din clienti
					response = newMessageHandle(
                                    i,
                                    read_fds,
                                    clients);
					if(response) {close(sockfd); return response;}
				}
			}
		}
    }
    close(sockfd);
    return 0;
}

int main(int argc, char *argv[])
{
    int                 response;
    int                 sockfd;
    //usage
    if(argc != 2) {
        std::cerr << "Wrong parameters. Usage: " << argv[0]<< " <port>" << std::endl;
        return 1;
    }
    //initializari
    response = init(argv[1], sockfd);
    if(response != 0) {
        return response;
    }
    //main loop
    response = loop(sockfd);
    if(response != 0) {
        return response;
    }

    return 0;
}


//functie care cauta un client dupa socket-ul pe care e activ si il elimina
//intoarce 1 daca nu gaseste clientul, 0 daca reuseste eliminarea
int removeClientBySocket(
                std::vector<Client> &clients,
                int                 socket)
{
    std::vector<Client>::iterator   it = clients.begin();
    for(; it != clients.end(); ++it) {
        if((*it).getSocket() == socket) break;
    }
    if(it == clients.end()) return 1;
    clients.erase(it);
    return 0;
}
//se ocupa de eventul de initializare, completeaza datele din vectorul de llienti
//pentru clientul care isi trimite numele si portul pe care asculta
int handleInit(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream  in(buffer);
    std::string         cmd;
    char                name[MAXLEN];
    char                listen_port[MAXLEN];
    char                sendBuf[MAXLEN];

    memset(sendBuf, 0, MAXLEN);
    in >> cmd;
    if(strcmp(cmd.c_str(), "init")) {
        std::cerr << "Initialization error" << std::endl;
        return -1;
    }
    //numele clientului
    in >> name;
    if(!(strlen(name) >= 1)) {
        std::cerr << "Invalid name" << std::endl;
        sprintf(sendBuf, "Fail");
        send(socket, sendBuf, strlen(sendBuf), 0);
        return -2;
    }
    //portul pe care asculta
    in >> listen_port;
    if(!(atoi(listen_port) >= 10000)) {
        std::cerr << "Invalid listen port." << std::endl;
        sprintf(sendBuf, "Fail");
        send(socket, sendBuf, strlen(sendBuf), 0);
        return -3;
    }
    //se cauta daca exista deja un client cu numele asta
    for(auto &c: clients) {
        if(!strcmp(c.getName().c_str(), name)) {
            sprintf(sendBuf, "Fail");
            std::cerr << "There is already a known client connected!"
                << " on this link. Terminating initialization." << std::endl;
            send(socket, sendBuf, sizeof(sendBuf), 0);
            return 2;
        }
    }

    //se cauta clientul si se actualizeaza datele
    for(auto &c: clients) {
        if(c.getSocket() != socket) continue;
        if(c.getName().size() != 0) {
            std::cerr << "There is already a known client connected!"
                << " on this link. Terminating initialization." << std::endl;
            sprintf(sendBuf, "Fail");
            send(socket, sendBuf, sizeof(sendBuf), 0);
            return 2;
        }
        c.getName().assign(name);
        c.getPort().assign(listen_port);
        std::cout << "Client data has been updated for client" << std::endl
                << "--------- Name: " << c.getName() << std::endl
                << "--------- Port: " << c.getPort() << std::endl
                << "--------- Socket: " << c.getSocket() << std::endl;
        sprintf(sendBuf, "Success");
        send(socket, sendBuf, sizeof(sendBuf), 0);
        return 0;
    }
    std::cerr << "No client with this socket was found. Database error."
              << std::endl;
    return 1;
}

//se ocupa de cererea infoclients
int handleInfoClients(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream  in(buffer);
    std::ostringstream  msg;
    std::string         cmd;
    char                sendBuffer[MAXLEN];
    in >> cmd;
    if(strcmp(cmd.c_str(), "infoclients")) {
        std::cerr << "Infoclients error." << std::endl;
        return -1;
    }
    //se parcurge fiecare client si se afiseaza informatiile sale
    for(auto &c: clients) {
        std::cout << "LOG:" << c.log() << std::endl;
        msg << c.log();
    }
    //se trimite raspunsul la client
    memset(sendBuffer, 0, MAXLEN);
    strncpy(sendBuffer, msg.str().c_str(), strlen(msg.str().c_str()));
    std::cout << "Sending response:" << std::endl
        << sendBuffer << std::endl
        << "On socket: " << socket << std::endl;
    send(socket, sendBuffer, strlen(sendBuffer  ), 0);
    return 0;
}

//se ocupa de cererea share
int handleShare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream  in(buffer);
    std::string         cmd;
    std::string         fname;
    long                fsize = 0;
    char                sendBuf[MAXLEN];

    in >> cmd;
    if(strcmp(cmd.c_str(), "share")) {
        std::cerr << "Share error" << std::endl;
        return -1;
    }
    //mesaj trimis eronat
    in >> fname >> fsize;
    if(!(fsize > -1 && fname.length() >= 1)) {
        std::cerr << "Invalid file name and size" << std::endl;
        return 1;
    }

    File f(fname.c_str(), fsize);
    //se cauta clientul corespunzator si se adauga fisierul in vectorul sau de fisiere 
    //partajate
    for(auto &c: clients) {
        if(c.getSocket() != socket) continue;
        c.getShared_files().push_back(f);
        std::cout << "Added to client: "
            <<c.log()
            <<" **** File: " << f.getName() << " "
            <<f.getSize() << std::endl;
    }
    //se trimite succes la client
    memset(sendBuf, 0, sizeof(sendBuf));
    sprintf(sendBuf, "Success");
    send(socket, sendBuf, sizeof(sendBuf), 0);
    return 0;
}

//se ocupa de cererea unshare
int handleUnshare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream          in(buffer);
    std::string                 cmd;
    std::string                 fname;
    char                        sendBuf[MAXLEN];
    int                         response;

    memset(sendBuf, 0, MAXLEN);

    in >> cmd;
    if(strcmp(cmd.c_str(), "unshare")) {
        std::cerr << "Unshare error" << std::endl;
        return -1;
    }
    //nume invalid
    in >> fname;
    if(fname.size() < 1) {
        sprintf(sendBuf, "-5");
        send(socket, sendBuf, sizeof(sendBuf), 0);
        return 1;
    }
    //se cauta clientul si elimina fisierul din lista de fisiere partajate
    for(auto &c: clients) {
        if(c.getSocket() != socket) continue;
        response = c.removeFile(fname);
        //nu s-a gasit fisierul
        if(response) {
            sprintf(sendBuf, "-5");
            send(socket, sendBuf, sizeof(sendBuf), 0);
            return 1;
        }
        std::cout << "Successfully removed file: "
            << fname << "  from client: "
            << c.log();
        //se raspunde afirmativ cllientului
        sprintf(sendBuf, "Success");
        send(socket, sendBuf, sizeof(sendBuf), 0);
        return 0;
    }

    return 2;
}

//se ocupa de comanda getshare
int handleGetShare(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream              in(buffer);
    std::ostringstream              msg;
    std::string                     cmd;
    std::string                     cname;
    char                            sendBuf[MAXLEN];
    std::vector<Client>::iterator   it;

    memset(sendBuf, 0, MAXLEN);

    in >> cmd;
    if(strcmp(cmd.c_str(), "getshare")) {
        std::cerr << "Unshare error" << std::endl;
        return -1;
    }

    //se cauta clientul
    in >> cname;
    for(it = clients.begin(); it != clients.end(); ++it) {
        if(!strcmp((*it).getName().c_str(), cname.c_str())) break;
    }
    // nu a fost gasit clientul, serverul intoarce -3
    if(it == clients.end()) {
        sprintf(sendBuf, "-3");
        send(socket, sendBuf, sizeof(sendBuf), 0);
        return 1;
    }
    //a fost gasit clientul, trimitem lista ceruta
    msg << (*it).getShared_files().size() << std::endl;
    for(File f: (*it).getShared_files()) {
        msg << f.getName() << " ";
        msg << f.translateSize() << std::endl;
    }
    std::cout << "Sending getshare response with: "
        << msg.str() << std::endl;
    strncpy(sendBuf, msg.str().c_str(), strlen(msg.str().c_str()));
    send(socket,sendBuf, sizeof(sendBuf), 0);
    return 0;
}


//se ocupa de comanda quit
int handleQuit(
    const char*         buffer,
    std::vector<Client> &clients,
    int                 socket)
{
    std::istringstream              in(buffer);
    std::string                     cmd;
    char                            sendBuf[MAXLEN];
    int                             response;

    memset(sendBuf, 0, MAXLEN);

    in >> cmd;
    if(strcmp(cmd.c_str(), "quit")) {
        std::cerr << "Quit error" << std::endl;
        return -1;
    }
    //clientul nu a fost gasit, o eroare de baza de date
    response = removeClientBySocket(clients, socket);
    if(response) {
        std::cerr << "Unable to find the quitting client.";
        sprintf(sendBuf, "-3");
        send(socket, sendBuf, sizeof(sendBuf), 0);
        return -2;
    }
    return 0;
}

//initializeaza map-ul folosit pentru switch-uri de comenzi
void initMap(std::map<std::string, int> &switchMap)
{
    switchMap.insert(std::make_pair("init", 0));
    switchMap.insert(std::make_pair("infoclients", 1));
    switchMap.insert(std::make_pair("getshare", 2));
    switchMap.insert(std::make_pair("share", 3));
    switchMap.insert(std::make_pair("unshare", 4));
    switchMap.insert(std::make_pair("quit", 5));
}

