//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <map>
#include <list>

#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#define MAX_CLIENTS	3
#define BUFLEN 1024
#define IPV4SIZE 32
#define MAXLEN 128
#define KiB 1024
#define MiB 1048576
#define GiB 1073741824
#define MAXTRANSFER 3

enum {
    cmd_initialize, cmd_infoclients, cmd_getshare, cmd_share, cmd_unshare, cmd_quit
};

struct share_file_transfer {
    std::ifstream   f;
    int             socket;
    std::string     file;
    bool            active;
};

struct get_file_transfer {
    std::ofstream   f;
    int             socket;
    std::string     client;
    std::string     file;
    bool            active;
};
