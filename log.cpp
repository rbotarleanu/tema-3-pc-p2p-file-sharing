//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "log.h"

Log::Log() {}

Log::Log(const char* client, const char* file) {
    this->file = file;
    this->client = client;
}

//constructor clonare
Log::Log(const Log& source) {
    this->file = source.file;
    this->client = source.client;
}

Log::~Log() {}

//copy assignment operator
Log&
Log::operator =(const Log &source) {
    //verific pentru asignare eronata
    if(this != &source) {
        this->file = source.file;
        this->client = source.client;
    }
    //intorc noua identiate
    return *this;
}

