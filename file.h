//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#pragma once
#include "utils.h"

class File {
private:
	//numele si dimesniunea
    std::string name;
    long fileSize;
public:
    File();
    File(const char *name, long fileSize);
    File(const File &source);
    File(const char *name, long fileSize, const char* size_unit);
    std::string &getName() { return name; }
    long getSize() const { return fileSize; }
    const char *translateSize() const;
    File& operator =(const File& source);
    bool  operator ==(const File& source);
};
